
Reset Credentials

```bash
rm config/credentials.yml.enc
```
```bash
rails secret
```
Add a new generated key
```
EDITOR='code --wait' rails credentials:edit
```

```yaml
# Other secrets
devise_jwt_secret_key: (copy and paste the generated secret here)
```


---
### Swagger doc available on:

http://0.0.0.0:3000/api-docs/index.html

* for authorization use token without "Bearer" prefix