# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

gem 'action_policy'
gem 'active_model_serializers', '~> 0.10.14'
gem 'bootsnap', '~> 1.16', require: false
gem 'devise', '~> 4.9'
gem 'devise-jwt', '~> 0.11.0'
gem 'faker', '~> 3.2'
gem 'jsonapi-serializer', '~> 2.2'
gem 'pagy', '~> 6.0'
gem 'pg', '~> 1.5.4'
gem 'puma', '~> 5.0'
gem 'rack-cors', '~> 2.0'
gem 'rails', '~> 7.0.8'
gem 'rswag-api', '~> 2.10'
gem 'rswag-ui', '~> 2.10'
gem 'rubocop', '~> 1.56'
gem 'sidekiq', '~> 7.1', '>= 7.1.5'
gem 'tzinfo-data', '~> 1.2023', platforms: %i[mingw mswin x64_mingw jruby]

group :development, :test do
  gem 'debug', '~> 1.8', platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem 'letter_opener', '~> 1.8'
end
