class RemoveNewColumnNameAndDescriptionFromTags < ActiveRecord::Migration[6.0] # Adjust the version as needed
  def change
    remove_columns :tags, :new_column_name, :description
  end
end
