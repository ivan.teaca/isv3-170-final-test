class CreateArticlesTagsJoinTable < ActiveRecord::Migration[7.0]
  def change
    create_table :articles_tags, id: false do |t|
      t.integer :article_id
      t.integer :tag_id
    end
  end
end
