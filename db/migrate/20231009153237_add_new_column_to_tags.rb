class AddNewColumnToTags < ActiveRecord::Migration[7.0]
  def change
    add_column :tags, :new_column_name, :string
  end
end
