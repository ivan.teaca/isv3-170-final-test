# frozen_string_literal: true

class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :tags, :user_id, :author_name, :created_at, :updated_at
  belongs_to :category

  def author_name
    object.user.full_name
  end
end
