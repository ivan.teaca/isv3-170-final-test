# frozen_string_literal: true

class CommentSerializer
  include JSONAPI::Serializer

  attributes :body

  belongs_to :user

  attribute :user_full_name do
    "#{object.user.first_name} #{object.user.last_name}"
  end
end
