# frozen_string_literal: true

class ArticlesController < ApplicationController
  include Pagy::Backend
  # before_action :authenticate_user!, except: %i[index show]

  def index
  per_page = params[:per_page].to_i || 10
  per_page = [per_page, 1].max

  @articles = Article.published # Filter by published articles

  if params[:category_id].present?
    category = Category.find(params[:category_id])
    @articles = @articles.where(category: category)
  end

  if params[:tag_name].present?
    Rails.logger.debug("Searching for articles with tag_name: #{params[:tag_name]}")
    @articles = @articles.joins(:tags).where(tags: { name: params[:tag_name] })
  end

  if params[:search].present?
    search_query = params[:search].strip
    @articles = @articles.where('title LIKE ? OR description LIKE ?', "%#{search_query}%", "%#{search_query}%")
  end

  if params[:start_date].present? && params[:end_date].present?
    start_date = Date.strptime(params[:start_date], '%d/%m/%Y').beginning_of_day.in_time_zone
    end_date = Date.strptime(params[:end_date], '%d/%m/%Y').end_of_day.in_time_zone
    @articles = @articles.where(created_at: start_date..end_date)
  end

  @pagy, @articles = pagy(@articles, items: per_page)

  response_data = {
    pagination: {
      current_page: @pagy.page,
      total_pages: @pagy.pages,
      next_page: @pagy.next,
      prev_page: @pagy.prev
    },
    data: @articles
  }

  render json: response_data, include: :tags
end
  def ban_author
    @user = User.find(params[:user_id])
    authorize! @user, to: :ban_author

    # Your ban author logic here
  end
  def unban_author
    @user = User.find(params[:user_id])
    authorize! @user, to: :unban_author

    # Your unban author logic here
  end
  def publish
    @article = Article.find(params[:id])
    @article.update(published: true)
    render json: @article
  end

  def hide
    @article = Article.find(params[:id])
    @article.update(published: false)
    render json: @article
  end

  def by_category
    category_id = params[:category_id]
    @articles = Article.where(category_id: category_id)
    render json: @articles
  end

  def show
    article = Article.find(params[:id])
    render json: article, serializer: ArticleSerializer
  end

  def create
    category_name = params[:category]
    category = Category.find_by(name: category_name)

    unless category
      render json: { error: "Category '#{category_name}' does not exist" }, status: :unprocessable_entity
      return
    end

    @article = Article.new(article_params)
    @article.category = category

    tag_names = params[:tag_names]
    tag_names&.each do |tag_name|
      tag = Tag.find_or_create_by(name: tag_name)
      @article.tags << tag
    end

    if @article.save
      render json: @article.as_json(include: :tags, except: :category_id), status: :created
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  def update
    @article = current_user.articles.find(params[:id])

    if @article.update(article_params)
      render json: @article
    else
      render json: { errors: @article.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def destroy
    @article = current_user.articles.find(params[:id])
    @article.destroy
    head :no_content
  end

  private

  def article_params
    params.require(:article).permit(:title, :description, :user_id, :published, :tag_id, category: [], tag_names: [])
  end
end
