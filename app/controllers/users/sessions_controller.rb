# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    include RackSessionsFix
    respond_to :json
    def create
      user = User.find_by(email: params[:user][:email])

      if user.nil?
        render json: { error: "User not found" }, status: :not_found
      elsif user.banned?
        render json: { error: "Access denied. You are banned." }, status: :unauthorized
      else
        if user.valid_password?(params[:user][:password])
          # Log the user in
        else
          render json: { error: "Invalid password" }, status: :unauthorized
        end
      end
    end

    private

    def respond_with(current_user, _opts = {})
      render json: {
        status: {
          code: 200, message: 'Logged in successfully.',
          data: { user: UserSerializer.new(current_user).serializable_hash[:data][:attributes] }
        }
      }, status: :ok
    end

    def respond_to_on_destroy
      if request.headers['Authorization'].present?
        jwt_payload = JWT.decode(request.headers['Authorization'].split(' ').last,
                                 Rails.application.credentials.devise_jwt_secret_key!).first
        current_user = User.find(jwt_payload['sub'])
      end

      if current_user
        render json: {
          status: 200,
          message: 'Logged out successfully.'
        }, status: :ok
      else
        render json: {
          status: 401,
          message: "Couldn't find an active session."
        }, status: :unauthorized
      end
    end
  end
end
