module Users
  class RegistrationsController < Devise::RegistrationsController
    include RackSessionsFix
    respond_to :json

    private

    def sign_up_params
      params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name)
    end

    def respond_with(current_user, _opts = {})
      if current_user.persisted?
        # Trigger email confirmation asynchronously using Sidekiq
        UserMailer.confirmation_email(current_user).deliver_later(queue: 'default', wait: 5.seconds)

        render json: {
          status: { code: 200, message: 'Signed up successfully.' },
          data: UserSerializer.new(current_user).serializable_hash[:data][:attributes]
        }
      else
        render json: {
          status: { message: "User couldn't be created successfully. #{current_user.errors.full_messages.to_sentence}" }
        }, status: :unprocessable_entity
      end
    end


  end
end
