# app/controllers/admin/users_controller.rb
class Admin::UsersController < ApplicationController
  before_action :set_user, only: %i[ban unban]

  # GET /admin/users
  def index
    @users = User.all
  end

  # POST /admin/users/:id/ban
  def ban
    if @user.update(banned: true)
      redirect_to admin_users_path, notice: 'User was successfully banned.'
    else
      redirect_to admin_users_path, alert: 'Unable to ban the user.'
    end
  end

  # POST /admin/users/:id/unban
  def unban
    if @user.update(banned: false)
      redirect_to admin_users_path, notice: 'User was successfully unbanned.'
    else
      redirect_to admin_users_path, alert: 'Unable to unban the user.'
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
