class ApplicationController < ActionController::API
  before_action :configure_permitted_parameters, if: :devise_controller?
  include Pagy::Backend

  # Set the time zone
  config.time_zone = 'Eastern European Time'

  protected

  # Define a method to check if a user is an admin.
  def admin_user?
    current_user&.admin?
  end

  # Include Action Policy and set up authorization for the admin role.
  include ActionPolicy::Controller
  authorize :admin, through: :current_user

  # Handle unauthorized access
  rescue_from ActionPolicy::Unauthorized do |exception|
    flash[:alert] = "You are not authorized to perform this action."
    redirect_to(request.referrer || root_path)
  end

  # ...

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[first_name last_name avatar])
    devise_parameter_sanitizer.permit(:account_update, keys: %i[first_name last_name avatar])
  end
end
