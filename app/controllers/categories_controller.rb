# frozen_string_literal: true

class CategoriesController < ApplicationController
  before_action :set_category, only: %i[show update destroy]

  def index
    @categories = Category.all
    render json: @categories
  end

  def show
    render json: @category
  end

  def create
    @category = Category.new(category_params)
    @category.name.downcase!

    if @category.save
      render json: @category, status: :created
    else
      custom_error_message = 'This category already exists!'
      render json: { errors: [custom_error_message] }, status: :unprocessable_entity
    end
  end

  def update
    if @category.update(category_params)
      @category.name.downcase!
      render json: @category
    else
      render json: { errors: @category.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def destroy
    @category.destroy
    head :no_content
  end

  def articles_by_category
    @category = Category.find(params[:id])
    @articles = @category.articles
    render json: @articles
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name)
  end
end
