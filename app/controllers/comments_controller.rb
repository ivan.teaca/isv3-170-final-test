# frozen_string_literal: true

class CommentsController < ApplicationController
  def index
    per_page = params[:per_page].to_i || 25
    per_page = [per_page, 1].max

    @comments = Comment.all

    @pagy, @comments = pagy(@comments, items: per_page)

    render json: @comments
  end

  def show
    @comment = Comment.find(params[:id])
    render json: @comment
  end

  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.build(comment_params)
    @comment.user_id = current_user.id

    if @comment.save
      render json: @comment, status: :created
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  def update
    @comment = Comment.find(params[:id])

    if @comment.update(comment_params)
      render json: @comment
    else
      render json: { errors: @comment.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    head :no_content
  end

  def search_by_user
    per_page = params[:per_page].to_i || 25
    per_page = [per_page, 1].max

    user_id = params[:user_id]
    @comments = Comment.where(user_id: user_id)

    @pagy, @comments = pagy(@comments, items: per_page)

    render json: @comments
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :user_id)
  end
end
