# app/workers/user_mailer_worker.rb

class UserMailerWorker
  include Sidekiq::Worker

  def perform(user_id)
    # Fetch the user record based on the user_id
    user = User.find(user_id)

    # Call the UserMailer method to send the confirmation email
    UserMailer.confirmation_email(user).deliver_now
  end
end
