class ApplicationMailer < ActionMailer::Base
  default from: "admin@final-test-blog.com"
  layout "mailer"
end
