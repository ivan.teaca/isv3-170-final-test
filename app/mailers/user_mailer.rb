# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def confirmation_email(user)
    @user = user
    mail(to: @user.email, subject: 'Registration Success: Welcome Aboard! | Final Test Blog')
  end
end
