# frozen_string_literal: true

class Category < ApplicationRecord
  validates :name, presence: true, uniqueness: { case_sensitive: false, message: 'This category already exist!' }
  has_many :articles
end
