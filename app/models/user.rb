class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :jwt_authenticatable, jwt_revocation_strategy: self

  validates :first_name, presence: true
  validates :last_name, presence: true
  has_many :articles, dependent: :destroy
  has_many :comments

  # Add the banned attribute
  attribute :banned, :boolean, default: false

  def full_name
    "#{first_name} #{last_name}"
  end
end
