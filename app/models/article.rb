# frozen_string_literal: true

class Article < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many :comments

  validates :title, presence: true
  validates :description, presence: true
  validates :category, presence: true

  validates_inclusion_of :published, in: [true, false]

  scope :published, -> { where(published: true) }
  scope :hidden, -> { where(published: false) }

  has_and_belongs_to_many :tags, join_table: :articles_tags
  scope :tagged_with, lambda { |tag_name|
    joins(:tags).where(tags: { name: tag_name })
  }
end
