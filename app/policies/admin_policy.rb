class AdminPolicy < ApplicationPolicy
  def ban_author?
    user&.admin?
  end

  def unban_author?
    user&.admin?
  end
end