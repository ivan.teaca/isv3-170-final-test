# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  namespace :admin do
    resources :users do
      member do
        post 'ban'
        post 'unban'
      end
    end
  end
  post '/users/:id/ban', to: 'users#ban', as: 'ban_user'
  resources :articles do
    member do
      post 'publish'
      post 'hide'
    end
  end
  devise_for :users, path: '', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    registration: 'signup'
  }, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  get '/comments/by_user', to: 'comments#search_by_user'
  resources :tags
  resources :articles do
    resources :comments, only: %i[index create]
  end
  resources :comments, only: %i[index show create destroy]
  resources :categories, only: %i[index show create destroy]
end
