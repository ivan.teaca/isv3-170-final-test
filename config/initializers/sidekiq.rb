# config/initializers/sidekiq.rb

Sidekiq.configure_server do |config|
  config.logger = ActiveSupport::Logger.new(Rails.root.join('log', 'sidekiq.log'))
end

Sidekiq.configure_client do |config|
  config.logger = ActiveSupport::Logger.new(Rails.root.join('log', 'sidekiq.log'))
end
